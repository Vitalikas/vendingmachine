package vendingMachine.model;

import java.math.BigDecimal;

public class Product implements Comparable<Product> {
    private final int code;
    private final String name;
    private final String size;
    private final BigDecimal price;
    private int inStock;

    public Product(int code, String name, String size, BigDecimal price, int inStock) {
        this.code = code;
        this.name = name;
        this.size = size;
        this.price = price;
        this.inStock = inStock;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    @Override
    public String toString() {
        return "Product{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", size='" + size + '\'' +
                ", price=" + price +
                ", inStock=" + inStock +
                '}';
    }

    @Override
    public int compareTo(Product other) {
        return this.code - other.code;
    }
}
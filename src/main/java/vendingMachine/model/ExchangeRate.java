package vendingMachine.model;

public enum ExchangeRate {
    EUR(1),
    USD(1.1843);

    private final double value;

    ExchangeRate(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }
}
package vendingMachine.model;

import java.math.BigDecimal;

public class Coin implements Comparable<Coin> {
    private final int code;
    private final BigDecimal value;
    private int count;

    public Coin(int code, BigDecimal value, int count) {
        this.code = code;
        this.value = value;
        this.count = count;
    }

    public int getCode() {
        return code;
    }

    public BigDecimal getValue() {
        return value;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Coin{" +
                "code=" + code +
                ", value=" + value +
                ", count=" + count +
                '}';
    }

    @Override
    public int compareTo(Coin other) {
        return this.getCode() - other.getCode();
    }
}
package vendingMachine.model;

import vendingMachine.service.DbService;
import vendingMachine.service.IoService;
import vendingMachine.service.SoundService;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

public class VendingMachine {
    Product selectedProduct = null;

    //private CurrencyType currency = CurrencyType.EUR;

    String ccyType = "EUR";

    double rate = 1.00;

    private final IoService ioService = new IoService();

    DecimalFormat df = new DecimalFormat("#0.00");

    DbService dbService = new DbService();

    SoundService soundService = new SoundService();

    // get data from database
    List<Product> products = dbService.getProducts();
    List<Coin> coins = dbService.getCoins();
    List<Coin> vmCoins = dbService.getVmCoins();

    // creating VM product stock
    public Map<Product, Integer> createProductStock(List<Product> products) {
        Map<Product, Integer> productStock = new HashMap<>();
        products.forEach(p -> productStock.put(new Product(p.getCode(), p.getName(), p.getSize(), p.getPrice(),
                        p.getInStock()), p.getInStock()));
        return productStock;
    }
    // initializing VM product stock
    Map<Product, Integer> productStock = createProductStock(products);

    // sorting VM product stock by code
    public TreeMap<Product, Integer> sortProductStock(Map<Product, Integer> productStock) {
        return new TreeMap<>(productStock);
    }

    // creating wallet coin stock
    public Map<Coin, Integer> createCoinStock(List<Coin> coins) {
        Map<Coin, Integer> coinStock = new HashMap<>();
        coins.forEach(coin -> coinStock.put(new Coin(coin.getCode(), coin.getValue(), coin.getCount()),
                coin.getCount()));
        return coinStock;
    }
    // initializing wallet coin stock
    Map<Coin, Integer> coinStock = createCoinStock(coins);

    // sorting wallet coin stock by code
    public TreeMap<Coin, Integer> sortCoinStock(Map<Coin, Integer> coinStock) {
        return new TreeMap<>(coinStock);
    }

    // creating vending machine coin stock
    public Map<Coin, Integer> createVmCoinStock(List<Coin> vmCoins) {
        Map<Coin, Integer> vmCoinStock = new HashMap<>();
        vmCoins.forEach(coin -> vmCoinStock.put(new Coin(coin.getCode(), coin.getValue(), coin.getCount()),
                coin.getCount()));
        return vmCoinStock;
    }

    // initializing vending machine coin stock
    Map<Coin, Integer> vmCoinStock = createVmCoinStock(vmCoins);

    // sorting vending machine coin stock by code
    public TreeMap<Coin, Integer> sortVmCoinStock(Map<Coin, Integer> vmCoinStock) {
        return new TreeMap<>(vmCoinStock);
    }

//    public void chooseCurrencyType() {
//        ioService.displayMessage("1 - EUR\n2 - USD\n3 - RUB\n0 - Cancel and exit\nSelect currency: ");
//        int input = ioService.validateCcyInput();
//
//        switch (input) {
//            case 0:
//                boolean ok = ioService.confirmInput("Are you sure you want to cancel?");
//                if (ok) {
//                    System.exit(0);
//                } else {
//                    chooseCurrencyType();
//                }
//            case 1:
//                currency = CurrencyType.EUR;
//                rate = 1.00;
//                break;
//            case 2:
//                currency = CurrencyType.USD;
//                rate = Double.parseDouble(dbService.getExchangeRate(String.valueOf(CurrencyType.USD)));
//                break;
//            case 3:
//                currency = CurrencyType.RUB;
//                rate = Double.parseDouble(dbService.getExchangeRate(String.valueOf(CurrencyType.RUB)));
//                break;
//        }
//    }

//    public List<CurrencyType> createCcyEnums(Set<String> ccyList) {
//        List<CurrencyType> ccyTypes = ccyList.stream()
//                .map(s -> CurrencyType.valueOf(s))
//                .collect(Collectors.toList());
//        return ccyTypes;
//    }

    public void selectCcy() {
        ioService.displayMessage("Select currency:");

        System.out.println(dbService.getCcyObj().keySet());

        String input = ioService.readInput();

        Map<String, String> ccyRates = dbService.getCcyObj();

        ccyRates.forEach((ccy, r) -> {
            if (input.equalsIgnoreCase(ccy)) {
                ccyType = ccy;
                rate = Double.parseDouble(r);
            }
        });

//        ccyList.forEach(ccy -> {
//            if (input.equalsIgnoreCase(ccy)) {
//                ccyTypes.forEach(ccyType -> {
//                    if (ccy.equalsIgnoreCase(String.valueOf(ccyType))) {
//                        currency = ccyType;
//                        rate = Double.parseDouble(dbService.getExchangeRate(ccy));
//                    }
//                });
//            }
//        });
    }

    public void displayMenu() {
        ioService.displayMessage("---------------------------------------------------");
        System.out.printf("%-5s %-15s %-8s %-8s %-5s %5s", "CODE", "PRODUCT", "SIZE", "PRICE", "CCY", "STOCK\n");
        ioService.displayMessage("---------------------------------------------------");

        // initializing sorted VM product stock
        TreeMap<Product, Integer> sortedProductStock = sortProductStock(productStock);

        sortedProductStock.forEach((p, inStock) -> {
                    System.out.printf("%-5d %-15s %-8s %-8s %-5s %5d",
                            p.getCode(),
                            p.getName(),
                            p.getSize(),
                            df.format(p.getPrice().multiply(BigDecimal.valueOf(rate))),
                            ccyType,
                            p.getInStock());
                    System.out.println();
                });

        ioService.displayMessage("\n0 - Cancel and exit\n---------------------------------------------------");
    }

    public Product selectProduct() {
        ioService.displayMessage("Choose product:");
        int option = ioService.validateProductInput(productStock);

        if (option == 0) {
            boolean ok = ioService.confirmInput("Are you sure you want to cancel?");
            if (ok) {
                System.exit(0);
            } else {
                return selectProduct();
            }
        }

        for (Map.Entry<Product, Integer> p : productStock.entrySet()) {
            if (option == p.getKey().getCode()) {
                if (p.getKey().getInStock() > 0) {
                    ioService.displayMessage("Selected: " +
                            p.getKey().getName().toUpperCase() + " "
                            + p.getKey().getSize() + " "
                            + df.format(p.getKey().getPrice().multiply(BigDecimal.valueOf(rate))) + ""
                            + ccyType);
                    selectedProduct = p.getKey();
                    if (selectedProduct.getName().equalsIgnoreCase("Coca-cola")) {
                        ioService.displayMessage("         __                              ___   __        .ama     ,");
                        ioService.displayMessage("      ,d888a                          ,d88888888888ba.  ,88\"I)   d");
                        ioService.displayMessage("     a88']8i                         a88\".8\"8)   `\"8888:88  \" _a8'");
                        ioService.displayMessage("   .d8P' PP                        .d8P'.8  d)      \"8:88:baad8P'");
                        ioService.displayMessage("  ,d8P' ,ama,   .aa,  .ama.g ,mmm  d8P' 8  .8'        88):888P'");
                        ioService.displayMessage(" ,d88' d8[ \"8..a8\"88 ,8I\"88[ I88' d88   ]IaI\"        d8[      ");
                        ioService.displayMessage(" a88' dP \"bm8mP8'(8'.8I  8[      d88'    `\"         .88       ");
                        ioService.displayMessage(",88I ]8'  .d'.8     88' ,8' I[  ,88P ,ama    ,ama,  d8[  .ama.g");
                        ioService.displayMessage("[88' I8, .d' ]8,  ,88B ,d8 aI   (88',88\"8)  d8[ \"8. 88 ,8I\"88[");
                        ioService.displayMessage("]88  `888P'  `8888\" \"88P\"8m\"    I88 88[ 8[ dP \"bm8m88[.8I  8[");
                        ioService.displayMessage("]88,          _,,aaaaaa,_       I88 8\"  8 ]P'  .d' 88 88' ,8' I[");
                        ioService.displayMessage("`888a,.  ,aadd88888888888bma.   )88,  ,]I I8, .d' )88a8B ,d8 aI");
                        ioService.displayMessage("  \"888888PP\"'        `8\"\"\"\"\"\"8   \"888PP'  `888P'  `88P\"88P\"8m\"");
                    }
                    return p.getKey();
                } else {
                    ioService.displayMessage("Not enough products in stock");
                    return this.selectProduct();
                }
            }
        }

        ioService.displayMessage("Wrong input! Try again");
        return this.selectProduct();
    }

    public void displayWallet() {
        ioService.displayMessage("Your wallet:");

        ioService.displayMessage("--------------------------");
        System.out.printf("%-5s %-7s %-5s %7s", "CODE", "VALUE", "CCY", "STOCK\n");
        ioService.displayMessage("--------------------------");

        // initializing sorted wallet coin stock
        TreeMap<Coin, Integer> sortedCoinStock = sortCoinStock(coinStock);

        sortedCoinStock.forEach(((coin, count) -> {
            System.out.printf("%-5d %-7s %-5s %6d",
                    coin.getCode(),
                    df.format(coin.getValue().multiply(BigDecimal.valueOf(rate))),
                    ccyType,
                    count);
            System.out.println();
        }));

        ioService.displayMessage("\n0 - Cancel");
    }

    public BigDecimal buyProduct(Product selectedProduct) {
        BigDecimal toPay = new BigDecimal(0);
        BigDecimal sum = new BigDecimal(0);
        int option;

        while (sum.compareTo(selectedProduct.getPrice().multiply(BigDecimal.valueOf(rate))) < 0) {
            ioService.displayMessage("Insert coin:");
            option = ioService.validateCoinInput(coinStock);

            for (Coin coin : coinStock.keySet()) {
                if (option == coin.getCode()) {
                    if (coinStock.get(coin) <= 0) {
                        ioService.displayMessage("You have no " +
                                df.format(coin.getValue().multiply(BigDecimal.valueOf(rate))) +
                                " coins to pay for! Try again");
                    } else {
                        sum = sum.add(coin.getValue().multiply(BigDecimal.valueOf(rate)));
                        ioService.displayMessage("Paid: " + df.format(sum) + ccyType);

                        soundService.playCoinInsertion();

                        toPay = selectedProduct.getPrice().multiply(BigDecimal.valueOf(rate)).subtract(sum);
                        ioService.displayMessage("Left to pay: " + df.format(toPay) + ccyType);

                        // update wallet
                        coinStock.computeIfPresent(coin, (k, v) -> v - 1);
                        // or coinStock.put(coin, coinStock.get(coin) - 1);
                        coin.setCount(coin.getCount() - 1);

                        // update vmCoinStock
                        for (Coin c : vmCoinStock.keySet()) {
                            if (c.getCode() == coin.getCode()) {
                                vmCoinStock.computeIfPresent(c, (k, v) -> v + 1);
                                c.setCount(c.getCount() + 1);
                            }
                        }

                        ioService.displayMessage(coinStock.get(coin).toString() + " coins of " +
                                df.format(coin.getValue().multiply(BigDecimal.valueOf(rate)))
                                + ccyType + " left");
                        displayWallet();
                    }
                } else if (option == 0) {
                    boolean ok = ioService.confirmInput("Are you sure you want to cancel payment?");
                    if (ok) {
                        coinStock = createCoinStock(coins);
                        ioService.displayMessage("Buying aborted");
                        start();
                    } else {
                        break;
                    }
                }
            }
        }

        // productStock.put(selectedProduct, productStock.get(selectedProduct) - 1);
        productStock.computeIfPresent(selectedProduct, (product, count) -> count - 1);
        selectedProduct.setInStock(selectedProduct.getInStock() - 1);

        ioService.displayMessage("Pick up your product and change!");

        return toPay;
    }

    public void payChange(BigDecimal change) {
        if (change.compareTo(new BigDecimal(0)) == 0) {
            ioService.displayMessage("No change to pay back");
        } else {
            ioService.displayMessage("Change: " + df.format(change.abs()) + ccyType);

            for (Coin c : vmCoinStock.keySet()) {
                if (change.abs().compareTo(c.getValue()) == 0) {
                    if (c.getCount() > 0) {
                        // update vending machine coin stock
                        vmCoinStock.computeIfPresent(c, (k, v) -> v - 1);
                        c.setCount(c.getCount() - 1);

                        // update wallet
                        for (Coin coin : coinStock.keySet()) {
                            if (coin.getCode() == c.getCode()) {
                                coinStock.computeIfPresent(coin, (k, v) -> v + 1);
                                coin.setCount(coin.getCount() + 1);
                            }
                        }
                    } else {
                        if (c.getValue().compareTo(BigDecimal.valueOf(0.20)) == 0) {
                            // 0.10Eur x 2 coins
                            for (Coin coin : vmCoinStock.keySet()) {
                                if (coin.getValue().compareTo(BigDecimal.valueOf(0.10)) == 0) {
                                    if (coin.getCount() > 1) {
                                        // update vending machine coin stock
                                        vmCoinStock.computeIfPresent(coin, (k, v) -> v - 2);
                                        coin.setCount(coin.getCount() - 2);
                                        // update wallet
                                        for (Coin wC : coinStock.keySet()) {
                                            if (wC.getCode() == coin.getCode()) {
                                                coinStock.computeIfPresent(wC, (k, v) -> v + 2);
                                                wC.setCount(wC.getCount() + 2);
                                            }
                                        }
                                    } else {
                                        ioService.displayMessage("There is no " + BigDecimal.valueOf(0.10) +
                                                " coin for change. Contact support!");
                                    }
                                }
                            }
                        } else {
                            throw new IllegalStateException("There is no coin for change. Contact support!");
                        }
                    }
                }
                // 0.30Eur = 0.20Eur + 0.10Eur or 3 x 0.10Eur
                else if (change.abs().compareTo(BigDecimal.valueOf(0.30)) == 0) {
                    // 0.20Eur + 0.10Eur coins for change
                    if (c.getValue().compareTo(BigDecimal.valueOf(0.20)) == 0) {
                        if (c.getCount() > 0) {
                            // 0.20Eur coin
                            // update vending machine coin stock
                            vmCoinStock.computeIfPresent(c, (k, v) -> v - 1);
                            c.setCount(c.getCount() - 1);
                            // update wallet
                            for (Coin coin : coinStock.keySet()) {
                                if (coin.getCode() == c.getCode()) {
                                    coinStock.computeIfPresent(coin, (k, v) -> v + 1);
                                    coin.setCount(coin.getCount() + 1);
                                }
                            }

                            // 0.10Eur coin
                            for (Coin coin : vmCoinStock.keySet()) {
                                if (coin.getValue().compareTo(BigDecimal.valueOf(0.10)) == 0) {
                                    if (coin.getCount() > 0) {
                                        // update vending machine coin stock
                                        vmCoinStock.computeIfPresent(coin, (k, v) -> v - 1);
                                        coin.setCount(coin.getCount() - 1);
                                        // update wallet
                                        for (Coin wC : coinStock.keySet()) {
                                            if (wC.getCode() == coin.getCode()) {
                                                coinStock.computeIfPresent(wC, (k, v) -> v + 1);
                                                wC.setCount(wC.getCount() + 1);
                                            }
                                        }
                                    } else {
                                        ioService.displayMessage("There is no " + BigDecimal.valueOf(0.10) +
                                                " coin for change. Contact support!");
                                    }
                                }
                            }
                        } else {
                            // 3 x 0.10Eur coins for change
                            for (Coin coin : vmCoinStock.keySet()) {
                                if (coin.getValue().compareTo(BigDecimal.valueOf(0.10)) == 0) {
                                    if (coin.getCount() > 2) {
                                        // update vending machine coin stock
                                        vmCoinStock.computeIfPresent(coin, (k, v) -> v - 3);
                                        coin.setCount(coin.getCount() - 3);
                                        // update wallet
                                        for (Coin wC : coinStock.keySet()) {
                                            if (wC.getCode() == coin.getCode()) {
                                                coinStock.computeIfPresent(wC, (k, v) -> v + 3);
                                                wC.setCount(wC.getCount() + 3);
                                            }
                                        }
                                    } else {
                                        ioService.displayMessage("There is no " + BigDecimal.valueOf(0.10) +
                                                " coin for change. Contact support!");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void start() {
        //chooseCurrencyType();
        //List<CurrencyType> ccyTypes = createCcyEnums(ccyList);
        selectCcy();
        displayMenu();
        Product selectedProduct = selectProduct();
        displayWallet();
        BigDecimal change = buyProduct(selectedProduct);
        payChange(change);
        soundService.playChangeAndProductPickup();
        dbService.writeProducts(productStock);
        dbService.writeCoins(coinStock);
        dbService.writeVmCoins(vmCoinStock);
        ioService.displayMessage("Data updated successfully");
    }
}
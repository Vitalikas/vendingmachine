package vendingMachine;

import vendingMachine.model.VendingMachine;

public class Main {
    public static void main(String[] args) {
        new VendingMachine().start();
    }
}
package vendingMachine.service;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import vendingMachine.model.Coin;
import vendingMachine.model.Product;
import java.io.*;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DbService {
    List<Product> products = null;
    List<Coin> coins = null;
    List<Coin> vmCoins = null;

    // Use Gson library for JSON parsing
    Gson gson = new GsonBuilder().setPrettyPrinting().create();

    // Getting JSON file path from folder "resources"
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    public List<Product> getProducts() {
        InputStream FILE_PATH = classLoader.getResourceAsStream("data/products.json");

        // Reading and converting JSON file to Java objects
        Type dataList = new TypeToken<List<Product>>(){}.getType();
        assert FILE_PATH != null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(FILE_PATH))) {
            products = gson.fromJson(reader, dataList);
        } catch (IOException e) {
            e.printStackTrace();
        } return products;
    }

    public List<Coin> getCoins() {
        // Getting JSON file path from folder "resources"
        InputStream FILE_PATH = classLoader.getResourceAsStream("data/wallet.json");

        // Reading and converting JSON file to Java objects
        Type dataList = new TypeToken<List<Coin>>(){}.getType();
        assert FILE_PATH != null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(FILE_PATH))) {
            coins = gson.fromJson(reader, dataList);
        } catch (IOException e) {
            e.printStackTrace();
        } return coins;
    }

    public List<Coin> getVmCoins() {
        // Getting JSON file path from folder "resources"
        InputStream FILE_PATH = classLoader.getResourceAsStream("data/vm-coins.json");

        // Reading and converting JSON file to Java objects
        Type dataList = new TypeToken<List<Coin>>(){}.getType();
        assert FILE_PATH != null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(FILE_PATH))) {
            vmCoins = gson.fromJson(reader, dataList);
        } catch (IOException e) {
            e.printStackTrace();
        } return vmCoins;
    }

    public void writeProducts(Map<Product, Integer> products) {
        List<Product> list = new ArrayList<>();
        products.forEach((key, value) -> list.add(key));

        String json = gson.toJson(list);
        try(
                OutputStream stream = Files.newOutputStream(Path.of("src/main/resources/data/products.json"));
                OutputStreamWriter writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8)
        ) {
            writer.write(json);
        } catch (Exception e) {
            System.out.println("Failed create/update file" + e);
        }
    }

    public void writeCoins(Map<Coin, Integer> coins) {
        List<Coin> list = new ArrayList<>();
        coins.forEach((key, value) -> list.add(key));

        String json = gson.toJson(list);
        try(
                OutputStream stream = Files.newOutputStream(Path.of("src/main/resources/data/wallet.json"));
                OutputStreamWriter writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8)
        ) {
            writer.write(json);
        } catch (Exception e) {
            System.out.println("Failed create/update file" + e);
        }
    }

    public void writeVmCoins(Map<Coin, Integer> vmCoins) {
        List<Coin> list = new ArrayList<>();
        vmCoins.forEach((key, value) -> list.add(key));

        String json = gson.toJson(list);
        try(
                OutputStream stream = Files.newOutputStream(Path.of("src/main/resources/data/vm-coins.json"));
                OutputStreamWriter writer = new OutputStreamWriter(stream, StandardCharsets.UTF_8)
        ) {
            writer.write(json);
        } catch (Exception e) {
            System.out.println("Failed create/update file" + e);
        }
    }

    public Map<String, String> getCcyObj() {
        Map<String, String> ccyRates = null;

        // Setting API URL, base ccy is EUR
        String API_PATH = "https://v6.exchangerate-api.com/v6/64523f9ea5a755990b5e0056/latest/EUR";

        try {
            // Making Request
            URL url = new URL(API_PATH);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            // Getting JSON data from API and converting to JSON object
            JsonObject jObj = (JsonObject) JsonParser.parseReader(
                    new InputStreamReader((InputStream) request.getContent()))
                    .getAsJsonObject().get("conversion_rates");
//            or
//            JsonElement e = JsonParser.parseReader(new InputStreamReader((InputStream) request.getContent()));
//            JsonObject o = e.getAsJsonObject().get("conversion_rates").getAsJsonObject();

            // Reading and converting JSON data to HashMap
            Type dataList = new TypeToken<Map<String, String>>(){}.getType();
            ccyRates = gson.fromJson(jObj, dataList);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ccyRates;
    }
}
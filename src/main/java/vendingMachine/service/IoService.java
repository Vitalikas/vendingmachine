package vendingMachine.service;

import vendingMachine.model.Coin;
import vendingMachine.model.CurrencyType;
import vendingMachine.model.Product;
import java.util.Map;
import java.util.Scanner;

public class IoService {
    private final Scanner scanner;

    public IoService() {
        this.scanner = new Scanner(System.in);
    }

    public void displayMessage(String message) {
        System.out.println(message);
    }

    public int validateCcyInput() {
        int number;
        int typeCount = CurrencyType.values().length;

        do {
            while (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.println(input + " is not a valid option! Please try again");
            }
            number = scanner.nextInt();
            if (number < 0) {
                System.out.println("Option must be positive! Please try again");
            } else if (number > typeCount) {
                System.out.println("There is no such option! Please try again");
            }
        } while (number < 0 || number > typeCount);

        return number;
    }

    public int validateProductInput(Map<Product, Integer> products) {
        int number;
        int productCount = products.size();

        do {
            while (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.println(input + " is not a valid option! Please try again");
            }
            number = scanner.nextInt();
            if (number < 0) {
                System.out.println("Option must be positive! Please try again");
            } else if (number > productCount) {
                System.out.println("There is no such option! Please try again");
            }
        } while (number < 0 || number > productCount);

        return number;
    }

    public boolean confirmInput(String text) {
        System.out.println(text + " [Y/N]");
        while (true) {
            String answer = scanner.next();
            if (answer.equalsIgnoreCase("Y")) {
                return true;
            } else if (answer.equalsIgnoreCase("N")) {
                return false;
            } else {
                System.out.println("Wrong input! Try again");
            }
        }
    }

    public int validateCoinInput(Map<Coin, Integer> coins) {
        int number;
        int coinQuantity = coins.size();

        do {
            while (!scanner.hasNextInt()) {
                String input = scanner.next();
                System.out.println(input + " is not a valid option! Please try again");
            }
            number = scanner.nextInt();
            if (number < 0) {
                System.out.println("Option must be positive! Please try again");
            } else if (number > coinQuantity) {
                System.out.println("There is no such option! Please try again");
            }
        } while (number < 0 || number > coinQuantity);

        return number;
    }

    public String readInput() {
        return scanner.next();
    }
}
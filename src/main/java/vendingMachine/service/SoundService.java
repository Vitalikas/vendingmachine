package vendingMachine.service;

import javax.sound.sampled.*;
import java.io.File;
import java.util.concurrent.CountDownLatch;

public class SoundService {
    public void playCoinInsertion() {
        final String PATH = "src/main/resources/sounds/coin-drop.wav";
        File audioFile = new File(PATH);
        try {
            // Create an AudioInputStream from a given sound file
            AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);

            // Obtain the Clip
            Clip clip = AudioSystem.getClip();

            // Open the AudioInputStream and start playing
            clip.open(audioStream);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playChangeAndProductPickup() {
        final String PATH = "src/main/resources/sounds/change-and-pickup.wav";
        File audioFile = new File(PATH);

        // create number of threads to execute before main thread continue to run
        CountDownLatch counter = new CountDownLatch(1);

        try(
                // Create an AudioInputStream from a given sound file
                AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile)) {

            // Obtain the Clip
            Clip clip = AudioSystem.getClip();

            // Add event listener. The counter decrease by 1 after event stops.
            clip.addLineListener(event -> {
                if (event.getType() == LineEvent.Type.STOP) {
                    counter.countDown();
                    clip.close();
                }
            });

            // Open the AudioInputStream and start playing
            clip.open(audioStream);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            // Awaiting for counter equals 0
            counter.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}